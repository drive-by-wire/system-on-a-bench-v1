import RPi.GPIO as GPIO
import enum
import signal

from Containers.Car import *
from Containers.TextPrint import *

SYSTEM_OFF_LED_CHANNEL = 23
SYSTEM_OFF_INPUT_CHANNEL = 24

LAPTOP_MODE_LED_CHANNEL = 27
LAPTOP_INPUT_CHANNEL = 22

JOYSTICK_MODE_LED_CHANNEL = 5
JOYSTICK_INPUT_CHANNEL = 6


def initLEDs():
    GPIO.setup(SYSTEM_OFF_LED_CHANNEL, GPIO.OUT)
    GPIO.setup(LAPTOP_MODE_LED_CHANNEL, GPIO.OUT)
    GPIO.setup(JOYSTICK_MODE_LED_CHANNEL, GPIO.OUT)
    
    GPIO.setup(JOYSTICK_INPUT_CHANNEL, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(LAPTOP_INPUT_CHANNEL, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(SYSTEM_OFF_INPUT_CHANNEL, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

def systemOffLED():
	GPIO.output(JOYSTICK_MODE_LED_CHANNEL, GPIO.LOW)
	GPIO.output(LAPTOP_MODE_LED_CHANNEL, GPIO.LOW)
	GPIO.output(SYSTEM_OFF_LED_CHANNEL, GPIO.HIGH)

def joystickLED():
	GPIO.output(JOYSTICK_MODE_LED_CHANNEL, GPIO.HIGH)
	GPIO.output(LAPTOP_MODE_LED_CHANNEL, GPIO.LOW)
	GPIO.output(SYSTEM_OFF_LED_CHANNEL, GPIO.LOW)

def laptopLED():
	GPIO.output(JOYSTICK_MODE_LED_CHANNEL, GPIO.LOW)
	GPIO.output(LAPTOP_MODE_LED_CHANNEL, GPIO.HIGH)
	GPIO.output(SYSTEM_OFF_LED_CHANNEL, GPIO.LOW)

def pollInputButtons(sigNum, stack_frame):
	global input_mode
	joystick_triggered = GPIO.input(JOYSTICK_INPUT_CHANNEL)
	laptop_triggered = GPIO.input(LAPTOP_INPUT_CHANNEL)

	if (joystick_triggered and laptop_triggered):
		return

	elif (joystick_triggered or laptop_triggered):
		input_mode = InputMode.JOYSTICK if joystick_triggered else InputMode.LAPTOP


def pollSystemOffButton(sigNum, stack_frame):
    global systemOffWasPressed
    systemOffWasPressed = GPIO.input(SYSTEM_OFF_INPUT_CHANNEL)


if __name__ == '__main__':

    input_mode = InputMode.SYSTEMOFF
    state = CarState.START
    systemOffWasPressed = 0

    GPIO.setmode(GPIO.BCM)

    initLEDs()
    systemOffLED()

    dT = 1/10
    signal.signal(signal.SIGALRM, pollInputButtons)
    signal.setitimer(signal.ITIMER_REAL, dT, dT)

    signal.signal(signal.SIGALRM, pollSystemOffButton)
    signal.setitimer(signal.ITIMER_REAL, dT, dT)

    while(1):
        if (state == CarState.START):
            if (input_mode != InputMode.SYSTEMOFF):
                state = CarState.OPERATION
                if (input_mode == InputMode.JOYSTICK):
                    joystickLED()
                if (input_mode == InputMode.LAPTOP):
                    laptopLED()

        elif (state == CarState.OPERATION):
            if (systemOffWasPressed == 1):
                state = CarState.ERROR
                systemOffLED()

        elif (state == CarState.ERROR):
            pass
