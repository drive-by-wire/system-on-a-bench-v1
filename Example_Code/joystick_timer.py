import os
# os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
import signal # Not supported on Windows if I recall correctly

def timerISR(sigNum, stack_frame): # Interrupt routine
    for event in pygame.event.get(): # User did something.
        if event.type == pygame.QUIT: # If user clicked close.
            done = True # Flag that we are done so we exit this loop.
    try:
        jid = joystick.get_instance_id()
    except AttributeError:
        # get_instance_id() is an SDL2 method
        jid = joystick.get_id()

    curr.steering_angle = joystick.get_axis(0)  * 25
    curr.percent_brake = (joystick.get_axis(4) + 1) * 50
    curr.percent_throttle = (joystick.get_axis(5) + 1) * 50
    curr.horn = "True" if joystick.get_button(0) == 1 else "False"
    curr.reverse = "True" if joystick.get_button(1) == 1 else "False"
    curr.windshield_wiper = "True" if joystick.get_button(2) == 1 else "False"
    curr.E_STOP = "True" if joystick.get_button(3) == 1 else "False"
    curr.left_turn = "True" if joystick.get_button(4) == 1 else "False"
    curr.right_turn = "True" if joystick.get_button(5) == 1 else "False"
    return None

class CarValues():
    """
    class CarValues():

    Defines the parameters needed for vehicle operation in a struct which can be passed around to the various functions that need it
    """
    def __init__(self):
        self.steering_angle = 0
        self.percent_brake = 0
        self.percent_throttle = 0
        self.horn = 0
        self.reverse = 0
        self.windshield_wiper = 0
        self.E_STOP = 0
        self.left_turn = 0
        self.right_turn = 0
        return

curr = CarValues()
pygame.init()

# Loop until the user clicks the close button.
done = False

# Initialize the joysticks.
pygame.joystick.init()

try:
    joystick = pygame.joystick.Joystick(0)
except:
    print("No joystick connected")
    quit()

joystick.init()

dT = 1/10 # Frequency of timer interrupt
signal.signal(signal.SIGALRM, timerISR) # Set Handler function for Timer ISR
signal.setitimer(signal.ITIMER_REAL, dT, dT) # Set interval for Timer ISR

# -------- Main Program Loop -----------
while True:

    print("Steering Angle: {:>6.3f}{}".format(curr.steering_angle, u"\N{DEGREE SIGN}"))
    print("Braking Percent: {:>6.3f}%".format(curr.percent_brake))
    print("Throttle Percent: {:>6.3f}%".format(curr.percent_throttle))
    print("Horn Bool: {}".format(curr.horn))
    print("Reverse Bool: {}".format(curr.reverse))
    print("Windshield Wiper Bool: {}".format(curr.windshield_wiper))
    print("E-STOP Bool: {}".format(curr.E_STOP))
    print("Left Turn Bool: {}".format(curr.left_turn))
    print("Right Turn Bool: {}".format(curr.right_turn))

pygame.quit()

