import pygame
import time

pygame.display.init() # Needed for some reason will have to look into why
pygame.joystick.init() # Initialize the joystick module

joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())] # Get list of joysticks
if pygame.joystick.get_count() == 0: # Error check
    print('No joystick detected')

joystick = joysticks[0] # Get joystick 0
print(joystick.get_name()) # Will have to define names of allowable joysticks
joystick.init() # Initialize the joystick for reading

while(1):
    pygame.event.pump() # Needed for some reason will have to look into why
    print(joystick.get_axis(0), joystick.get_axis(1), joystick.get_axis(2), joystick.get_axis(3)) # print axes values
    time.sleep(0.5) # Pause stream