import signal # Not supported on Windows if I recall correctly

def timerISR(sigNum, stack_frame): # Interrupt routine
    # do Stuff
    return None

dT = 1/50 # Frequency of timer interrupt
signal.signal(signal.SIGALRM, timerISR) # Set Handler function for Timer ISR
signal.setitimer(signal.ITIMER_REAL, dT, dT) # Set interval for Timer ISR


