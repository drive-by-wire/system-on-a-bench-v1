import board
import busio
import adafruit_mcp4725

# sudo pip install adafruit-circuitpython-mcp4725

i2c = busio.I2C(board.SCL, board.SDA)
dac = adafruit_mcp4725.MCP4725(i2c, address=0x60) # or whatever the i2c is

dac.value = 32767