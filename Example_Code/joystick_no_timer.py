#need to apply scaling function to make the throttle/braking/steering angle non-linear

import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame

# Define some colors.
BLACK = pygame.Color('black')
WHITE = pygame.Color('white')


# This is a simple class that will help us print to the screen.
# It has nothing to do with the joysticks, just outputting the
# information.
class TextPrint(object):
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def tprint(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, (self.x, self.y))
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10


class CarValues():
    """
    class CarValues():

    Defines the parameters needed for vehicle operation in a struct which can be passed around to the various functions that need it
    """
    def __init__(self):
        self.steering_angle = 0
        self.percent_brake = 0
        self.percent_throttle = 0
        self.horn = 0
        self.reverse = 0
        self.windshield_wiper = 0
        self.E_STOP = 0
        self.left_turn = 0
        self.right_turn = 0
        return

curr = CarValues()

pygame.init()

# Set the width and height of the screen (width, height).
screen = pygame.display.set_mode((500, 700))
pygame.display.set_caption("Vehicle Dashboard")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates.
clock = pygame.time.Clock()

# Get ready to print.
textPrint = TextPrint()

# Initialize the joysticks.
pygame.joystick.init()

try:
    joystick = pygame.joystick.Joystick(0)
except:
    print("No joystick connected")
    quit()

joystick.init()

wait_for_user = True
while (wait_for_user):
    for event in pygame.event.get():
        if event.type == pygame.JOYAXISMOTION or \
           event.type == pygame.JOYBALLMOTION or \
           event.type == pygame.JOYBUTTONDOWN or \
           event.type == pygame.JOYBUTTONUP or \
           event.type == pygame.JOYHATMOTION:
           
           wait_for_user = False

# -------- Main Program Loop -----------
while not done:

    for event in pygame.event.get(): # User did something.
        if event.type == pygame.QUIT: # If user clicked close.
            done = True # Flag that we are done so we exit this loop.

    #
    # DRAWING STEP
    #
    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(WHITE)
    textPrint.reset()

    try:
        jid = joystick.get_instance_id()
    except AttributeError:
        # get_instance_id() is an SDL2 method
        jid = joystick.get_id()

    textPrint.tprint(screen, "Joystick Commands")
    textPrint.indent()

    curr.steering_angle = joystick.get_axis(0)  * 25
    curr.percent_brake = (joystick.get_axis(4) + 1) * 50
    curr.percent_throttle = (joystick.get_axis(5) + 1) * 50
    curr.horn = "True" if joystick.get_button(0) == 1 else "False"
    curr.reverse = "True" if joystick.get_button(1) == 1 else "False"
    curr.windshield_wiper = "True" if joystick.get_button(2) == 1 else "False"
    curr.E_STOP = "True" if joystick.get_button(3) == 1 else "False"
    curr.left_turn = "True" if joystick.get_button(4) == 1 else "False"
    curr.right_turn = "True" if joystick.get_button(5) == 1 else "False"

    textPrint.tprint(screen, "Steering Angle: {:>6.3f}{}".format(curr.steering_angle, u"\N{DEGREE SIGN}"))
    textPrint.tprint(screen, "Braking Percent: {:>6.3f}%".format(curr.percent_brake))
    textPrint.tprint(screen, "Acceleration Percent: {:>6.3f}%".format(curr.percent_throttle))
    textPrint.tprint(screen, "Horn Bool: {}".format(curr.horn))
    textPrint.tprint(screen, "Reverse Bool: {}".format(curr.reverse))
    textPrint.tprint(screen, "Windshield Wiper Bool: {}".format(curr.windshield_wiper))
    textPrint.tprint(screen, "E-STOP Bool: {}".format(curr.E_STOP))
    textPrint.tprint(screen, "Left Turn Bool: {}".format(curr.left_turn))
    textPrint.tprint(screen, "Right Turn Bool: {}".format(curr.right_turn))

    textPrint.unindent()

    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # Limit to 20 frames per second.
    clock.tick(20)

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()
