import smbus

bus = smbus.SMBus(1)

while True:
    for port in [0x60, 0x68, 0x69]: #List of I2C ports to check
        try:
            bus.read_byte(port) # Check if port is connected
        except:
            print('Port 0x{:x} disconnected'.format(port))