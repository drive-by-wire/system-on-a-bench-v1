import time
import RPi.GPIO as GPIO
import math

####### defines #######
tpm = 4000 # Ticks/mile  Will need to test on actual vehicle


####### Global variables ######
dT = 0 # Time between pulses
prevTime = 0 # Previous ISR time
flag = False # for testing purposes to not overload the terminal

def spedometer_ISR():
    global dT, prevTime, flag
    curTime = time.thread_time_ns()
    dT = (curTime - prevTime) / math.pow(10, 9)


def main():
    global dT, flag
    pinInt = 1
    pinPWM = 12
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pinInt, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Interrupt pin
    GPIO.setup(pinPWM, GPIO.OUT)

    GPIO.add_event_detect(pinInt, GPIO.RISING, callback=spedometer_ISR) # Enable ISR

    pwm = GPIO.PWM(pinPWM, 500) # PWM for testing. Set second value to the frequency
    pwm.start(50)

    while True:
        if flag:
            speed = 3600 / (dT * tpm)
            print(speed + 'MPH')


if __name__=="__main__":
    main()