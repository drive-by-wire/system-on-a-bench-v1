import socket              

'''import socket

HOST = '10.0.0.3'  # The server's hostname or IP address
PORT = 1237        # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(b'Hello, world')
    data = s.recv(1024)

print('Received', repr(data))'''


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)       
host = '10.0.0.3'             # Name of Server we're connecting to
port = 1237                # Set port #

s.connect((host, port)) # Client version of bind()
                        # bind() should already have run on server-side

x = input()
usercommand = x.encode()
print(usercommand)
s.send(usercommand) #user commands go here
msg = s.recv(1024)
s.close()

print ("received data:", msg)