import enum

class CarValues():
	"""
	class Values():

	Defines the parameters needed for vehicle operation in a struct which can be passed around to the various functions that need it
	"""
	def __init__(self):
		self.steering_angle = 0
		self.percent_brake = 0
		self.percent_throttle = 0
		self.horn = 0
		self.reverse = 0
		self.windshield_wiper = 0
		self.E_STOP = 0
		self.left_turn = 0
		self.right_turn = 0
		return

class CarState(enum.Enum):
	"""
	class CarState(enum.Enum):

	Enumeration class for the main vehicle state machine
	"""
	START = enum.auto()
	SETUP = enum.auto()
	OPERATION = enum.auto()
	ERROR = enum.auto()

class InputMode(enum.Enum):
	"""
	class InputMode(enum.Enum):

	Enumeration class for the specific input type
	"""
	SYSTEMOFF = enum.auto()
	JOYSTICK = enum.auto()
	LAPTOP = enum.auto()
