import pygame 
import RPi.GPIO as GPIO
from Containers.Car import *
from Containers.TextPrint import *
from Containers.LEDs import *
import enum
import signal
import time

current_state = CarState.START
input_mode = InputMode.SYSTEMOFF
in_motion = False
reset_was_pressed = False
system_OFF = 0

def updateThrottle (current, prev):
    """
    Commands the accelerator using a DAC
    Author: Arturo Gamboa-Gonzalez

    :param current: container of the new desired values
    :param prev: container of the previous desired values
    :return: success if DAC was connected, failure if disconnnect
    """
    if ( abs (current.percent_throttle - prev.percent_throttle) > 0.01):
        print('setting throttle')
        #set voltage on DAC

    return "success"

def updateBraking (current, prev):
    """
    Commands the brake actuator to the desired position using a feedback loop
    Author: Arturo Gamboa-Gonzalez

    :param current: container of the new desired values
    :param prev: container of the previous desired values
    :return: success if actuator was connected, failure if disconnnect
    """
    if ( abs (current.percent_brake - prev.percent_brake) > 0.01):
        print('setting braking')
        #run control loop

    return "success"

def updateSteering (current, prev):
    """
    Commands the steering actuator to the desired position using a feedback loop
    Author: Arturo Gamboa-Gonzalez

    :param current: container of the new desired values
    :param prev: container of the previous desired values
    :return: success if actuator was connected, failure if disconnnect
    """
    if ( abs (current.steering_angle - prev.steering_angle) > 0.01): 
        print('setting steering')
        #run control loop

    return "success"

def updateLeftTurnSignal(current):
    return

def updateRightTurnSignal(current):
    return

def updateHorn(current):
    return

def updateWindShieldWipers(current):
    return



def pollButtons(sigNum, stack_frame):
    global input_mode
    global system_OFF
    global reset_was_pressed
    joystick_triggered = GPIO.input(JOYSTICK_INPUT_CHANNEL)
    laptop_triggered = GPIO.input(LAPTOP_INPUT_CHANNEL)
    
    if (GPIO.input(SYSTEM_OFF_INPUT_CHANNEL) == 1):
        system_OFF = True
    
    if (GPIO.input(RESET_INPUT_CHANNEL) == 1):
        reset_was_pressed = True
    
    if (joystick_triggered and laptop_triggered):
        return

    elif (joystick_triggered or laptop_triggered):
        input_mode = InputMode.JOYSTICK if joystick_triggered else InputMode.LAPTOP

# interrupt to update vehicle speed 
def pollSpeedometer():
    global in_motion
    return


if __name__ == '__main__':
    """
    Runs the state machine to control car, run at startup
    Authors: Arturo Gamboa-Gonzalez, Sutter Lum, Sriram Venkataraman
    """
    # create the containers for values
    new_vals = CarValues()
    prev_vals = CarValues()

    # joystick & display setup
    pygame.init()
    screen = pygame.display.set_mode((500, 700))
    pygame.display.set_caption("Vehicle Dashboard")
    textPrint = TextPrint()

    GPIO.setmode(GPIO.BCM)
    initLEDs()
    systemOffLED()

    # timer to read input buttons
    dT = 1/10
    signal.signal(signal.SIGALRM, pollButtons)
    signal.setitimer(signal.ITIMER_REAL, dT, dT)


    while (1):

        ###########################################################################################
        ### START State: Vehicle stays here until a switch is pressed (indicated by input_mode) ###
        ###########################################################################################
        
        if (current_state == CarState.START):

            if (input_mode != InputMode.SYSTEMOFF):
                if (in_motion == True):
                    current_state = CarState.ERROR
                    systemOffLED()

                else:
                    current_state = CarState.SETUP

        ################################################################################
        ### SETUP State: Perform necessary actions to put vehicle in OPERATION state ###
        ################################################################################
        
        elif (current_state == CarState.SETUP):
            error = False

            if (input_mode == InputMode.JOYSTICK):
                pygame.joystick.init()
                try:
                    joystick = pygame.joystick.Joystick(0)
                    joystick.init()
                except:
                    print("No joystick connected")
                    error = True

            # check power on HV step down circuit, if isolation fault error = 1 
            # check if joystick present in joystick mode, if not present error = 1 
            # check if laptop present in laptop mode, if not present error = 1 
            # run brake actuator test, if no response error = 1
            # run steering actuator test, if no response error = 1
            # run DAC test, if no response error = 1

            if (error == False):
                current_state = CarState.OPERATION

                if (input_mode == InputMode.JOYSTICK):
                    joystickLED()

                elif (input_mode == InputMode.LAPTOP):
                    laptopLED()

            else:
                current_state = CarState.ERROR
                systemOffLED()


        #######################################################################################
        ### OPERATION State: The vehicle will remain in this state under nominal conditions ###
        #######################################################################################
        
        elif (current_state == CarState.OPERATION):

            # check if the laptop is connected, and parse the values
            if (input_mode == InputMode.LAPTOP):
                pass

            # check if the joystick is connected, and parse the values
            if (input_mode == InputMode.JOYSTICK):
                try:
                    testConnection = pygame.joystick.Joystick(0)
                except:
                    print("No joystick connected")
                    current_state = CarState.ERROR

                pygame.event.get()

                new_vals.steering_angle = joystick.get_axis(0)  * 25
                new_vals.percent_brake = (joystick.get_axis(2) + 1) * 50
                new_vals.percent_throttle = (joystick.get_axis(5) + 1) * 50
                new_vals.horn = "True" if joystick.get_button(0) == 1 else "False"
                new_vals.reverse = "True" if joystick.get_button(1) == 1 else "False"
                new_vals.windshield_wiper = "True" if joystick.get_button(2) == 1 else "False"
                new_vals.E_STOP = "True" if joystick.get_button(3) == 1 else "False"
                new_vals.left_turn = "True" if joystick.get_button(4) == 1 else "False"
                new_vals.right_turn = "True" if joystick.get_button(5) == 1 else "False"

            if (new_vals.E_STOP == "True"):
                current_state = CarState.ERROR
                systemOffLED()
                continue
            
            if (updateThrottle(new_vals, prev_vals) == "failure"):
                current_state = CarState.ERROR
                systemOffLED()
                continue
            
            if (updateBraking(new_vals, prev_vals) == "failure"):
                current_state = CarState.ERROR
                systemOffLED()
                continue
            
            if (updateSteering(new_vals, prev_vals) == "failure"):
                current_state = CarState.ERROR
                systemOffLED()
                continue

            updateLeftTurnSignal(new_vals)

            updateRightTurnSignal(new_vals)
            
            updateHorn(new_vals)

            updateWindShieldWipers(new_vals)

            prev_vals = new_vals


        #########################################################################################
        ### ERROR State: The system will remain idle in this state until the reset is pressed ###
        #########################################################################################
        
        elif (current_state == CarState.ERROR):
            # join the threads
            if (reset_was_pressed == True):
                print('resetting')
                current_state = CarState.START
                input_mode = InputMode.SYSTEMOFF
                reset_was_pressed = False
                new_vals = CarValues()
                prev_vals = CarValues()


        #####################################################
        ### Update the display with pertinent information ###
        #####################################################
        
        screen.fill(pygame.Color('white'))
        textPrint.reset()
        textPrint.tprint(screen, "Current State: {}".format(current_state))
        textPrint.tprint(screen, "Reference Commands")
        textPrint.tprint(screen, "Steering Angle: {:>6.3f}{}".format(new_vals.steering_angle, u"\N{DEGREE SIGN}"))
        textPrint.tprint(screen, "Braking Percent: {:>6.3f}%".format(new_vals.percent_brake))
        textPrint.tprint(screen, "Acceleration Percent: {:>6.3f}%".format(new_vals.percent_throttle))
        textPrint.tprint(screen, "Horn Bool: {}".format(new_vals.horn))
        textPrint.tprint(screen, "Reverse Bool: {}".format(new_vals.reverse))
        textPrint.tprint(screen, "Windshield Wiper Bool: {}".format(new_vals.windshield_wiper))
        textPrint.tprint(screen, "E-STOP Bool: {}".format(new_vals.E_STOP))
        textPrint.tprint(screen, "Left Turn Bool: {}".format(new_vals.left_turn))
        textPrint.tprint(screen, "Right Turn Bool: {}".format(new_vals.right_turn))
        textPrint.unindent()
        pygame.display.flip()
        time.sleep(0.1)

    pygame.quit()